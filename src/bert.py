import numpy as np
import pandas as pd
# import transformers as ppb # pytorch transformers
import torch
from transformers import BertTokenizer, BertModel
tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
model = BertModel.from_pretrained('bert-base-uncased',
                                  output_hidden_states = True, # Whether the model returns all hidden-states.
                                  )



import rnnmath as rm
from utils import *

def view_data():
    """just observational """

    vocab_size = 2000

    vocab = pd.read_table("./data/vocab.wiki.txt", header=None, sep="\s+", index_col=0, names=['count', 'freq'], )
    num_to_word = dict(enumerate(vocab.index[:vocab_size]))
    word_to_num = invert_dict(num_to_word)
    
    # calculate loss vocabulary words due to vocab_size
    fraction_lost = rm.fraq_loss(vocab, word_to_num, vocab_size)
    print("Retained %d words from %d (%.02f%% of all tokens)\n" % (vocab_size, len(vocab), 100*(1-fraction_lost)))

    sents = load_np_dataset('./data/wiki-train.txt')

    print(sents[0:10])
    S_test = docs_to_indices(sents, word_to_num, 0, 0)
    X_test, D_test = seqs_to_npXY(S_test)





def use_bert():
    corpa=["the sri NNP NN is the country 's top NN .", "i live in the city"]


    for text in corpa:
        # Add the special tokens.
        marked_text = "[CLS] " + text + " [SEP]"

        # Split the sentence into tokens.
        tokenized_text = tokenizer.tokenize(marked_text)

        print(tokenized_text)

        # Map the token strings to their vocabulary indeces.
        indexed_tokens = tokenizer.convert_tokens_to_ids(tokenized_text)

        segments_ids = [1] * len(tokenized_text)

        tokens_tensor = torch.tensor([indexed_tokens])
        segments_tensors = torch.tensor([segments_ids])


        with torch.no_grad():

            outputs = model(tokens_tensor, segments_tensors)
            hidden_states = outputs[2][1:]

        token_embeddings = hidden_states[-1]

        token_embeddings = torch.squeeze(token_embeddings, dim=0)

        emb_list = [token_embed.tolist() for token_embed in token_embeddings]

        print(len(emb_list[0]))


    # print(storage)
    # print(len(storage[0])/5)


use_bert()