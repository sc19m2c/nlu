import subprocess



def q2_a():
    """
    learning rate: 0.5, 0.1, or 0.05 number of hidden units: 25 or 50
    number of steps to look back in truncated backpropagation: 0, 2, or 5
    """

    lr = [0.5, 0.1, 0.05]
    hdn = [25, 50]
    lb = [0,2,5]
    seeds = [2018, 157, 19, 3035]

    for lr_i in lr:
        for hdn_i in hdn:
            for lb_i in lb:
                for seed in seeds:
                    subprocess.run(["python", "rnn.py", "train-lm", "./data", f'{hdn_i}', f'{lb_i}', f'{lr_i}', f'./results/question2_a/all_final_results', f'{seed}'])
            




def q3_b():
    """
    learning rate: 0.5, 0.1, or 0.05 number of hidden units: 25 or 50
    number of steps to look back in truncated backpropagation: 0, 2, or 5
    """

    lr = [0.5, 0.1, 0.05]
    hdn = [25, 50]
    lb = [0,2,5]
    seeds = [2018, 157, 19, 3035]

    for lr_i in lr:
        for hdn_i in hdn:
            for lb_i in lb:
                for seed in seeds:
                    subprocess.run(["python", "rnn.py", "train-np", "./data", f'{hdn_i}', f'{lb_i}', f'{lr_i}', f'./results/q3b/all_final_results', f'{seed}'])


# q2_a()
# q3_b()


def run_lm():

    params = [
        [25, 0, 0.5],
        [25, 5, 0.5],
        [50, 0, 0.5],
    ]

    for i in range(len(params)):
        subprocess.run(["python", "rnn.py", "train-lm", "./data", f'{params[i][0]}', f'{params[i][1]}', f'{params[i][2]}', f'{params[i][0]}_{params[i][1]}_{params[i][2]}_rnn', f'{2018}'])


def run_all_final_models():
    #lm - 50 0 0,5  - record loss per iteration - save model per epoch 
    #np - 50 0/2/5 0.5  - record acc per iteration - save model per epoch
    #all untill fail
    # subprocess.run(["python", "rnn.py", "train-lm", "./data", f'{10}', f'{2}', f'{0.5}', f'', f'{2018}'])
    # subprocess.run(["python", "rnn.py", "train-lm", "./data", f'{10}', f'{5}', f'{0.5}', f'', f'{2018}'])

    # subprocess.run(["python", "rnn.py", "train-np", "./data", f'{50}', f'{2}', f'{0.5}', f'./results/q3b/final_model/model', f'{1000}'])
    # subprocess.run(["python", "rnn.py", "train-np", "./data", f'{50}', f'{0}', f'{0.5}', f'', f'{50}'])
    # subprocess.run(["python", "rnn.py", "train-np", "./data", f'{50}', f'{2}', f'{0.5}', f'', f'{50}'])
    # subprocess.run(["python", "rnn.py", "train-np", "./data", f'{50}', f'{5}', f'{0.5}', f'', f'{50}'])

    return 






# run_all_final_models()
# run_lm()