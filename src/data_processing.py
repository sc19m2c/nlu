
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def data_balance():
    df = pd.read_csv('./data/wiki-train.txt', sep='\t')
  
    
    total = len(df['verb_pos'].values)
    
    vbz = len(df.loc[df['verb_pos'] == 'VBZ']['verb_pos'].values)

    vbp = len(df.loc[df['verb_pos'] == 'VBP']['verb_pos'].values)
    
    print("vbz",vbz/total )
    print("vbp",vbp/total )
    return max(vbz/total, vbp/total)


def data_balance2(file):
    try:
        df = pd.read_csv(file, sep='\t')

        total = len(df['verb_pos'].values)
        
        vbz = len(df.loc[df['verb_pos'] == 'VBZ']['verb_pos'].values)
    except:
        return np.nan

    return vbz/total




def group_dependcy_ranges():

    "split the data set into multiple set based on the length of the subject the "

    df = pd.read_csv('./data/wiki-train.txt', sep='\t')
    sbj = df["subj_idx"]
    vrb = df["verb_idx"]
    df["dependcy_range"] = vrb-sbj
    


    for r in list(df["dependcy_range"].unique()):
        sdf = df.loc[df['dependcy_range'] == r]
        sdf = sdf.drop(columns=['dependcy_range'])
        # sdf.to_csv(f'./data/modified//test/dep_range_{r}.txt', index=False, sep='\t')

def plot_hists():

    "split the data set into multiple set based on the length of the subject the "

    df = pd.read_csv('./data/wiki-train.txt', sep='\t')
    sbj = df["subj_idx"]
    vrb = df["verb_idx"]
    df["dependcy_range"] = vrb-sbj
    
   
    ranges = df['dependcy_range'].value_counts().sort_index()
    print(ranges)
    ranges.plot.bar()
    plt.yscale('log')
    plt.xlabel("Dependency Range")
    plt.ylabel("Frequency")
    plt.show()

  

# plot_hists()


# group_dependcy_ranges()


def average_from_seeds():
    df = pd.read_csv('./results/question2_a/all_final_results.csv', header=None)
    df.columns = ["seed", "hdim", "lb", "lr", "loss", "unadjusted", "adjusted", "adjusted_adv"]
    print(df.head())

    lr = [0.5, 0.1, 0.05]
    hdn = [25, 50]
    lb = [0,2,5]

    df_out = pd.DataFrame(columns=["hdim", "lb", "lr", "loss_mean","loss_std","loss_high","loss_low", "unadjusted", "adjusted", "adjusted_adv"])

    for lr_i in lr:
        for hdn_i in hdn:
            for lb_i in lb:

                group = df.loc[(df['hdim'] == hdn_i) & (df['lb'] == lb_i) & (df['lr'] == lr_i)]
                mean_loss = group["loss"].mean()
                std_loss = group["loss"].std()
                high_loss = group["loss"].max()
                low_loss = group["loss"].min()

                ave_unadjusted = group["unadjusted"].mean()
                ave_adjusted = group["adjusted"].mean()
                ave_adjusted_adv = group["adjusted_adv"].mean()

                df_out = df_out.append({"hdim":hdn_i, "lb":lb_i, "lr":lr_i, "loss_mean":ave_loss,"loss_std":std_loss,"loss_high":high_loss,"loss_low":low_loss, "unadjusted":ave_unadjusted, "adjusted":ave_adjusted, "adjusted_adv":ave_adjusted_adv}, ignore_index=True)

               

    print(df_out)

    df_out.to_csv('./results/question2_a/average_results.csv', index=False)
    
    
def average_from_seeds_3b():
    df = pd.read_csv('./results/q3b/all_final_results.csv', header=None)
    df.columns = ["seed", "hdim", "lb", "lr", "loss", "acc"]
    print(df.head())

    lr = [0.5, 0.1, 0.05]
    hdn = [25, 50]
    lb = [0,2,5]

    df_out = pd.DataFrame(columns=["hdim", "lb", "lr", "low_acc","high_acc","mean_acc","std_acc"])

    for lr_i in lr:
        for hdn_i in hdn:
            for lb_i in lb:

                group = df.loc[(df['hdim'] == hdn_i) & (df['lb'] == lb_i) & (df['lr'] == lr_i)]
                mean_acc = group["acc"].mean()
                std_acc = group["acc"].std()
                high_acc = group["acc"].max()
                low_acc = group["acc"].min()

                df_out = df_out.append({"hdim":hdn_i, "lb":lb_i, "lr":lr_i, "mean_acc":mean_acc,"std_acc":std_acc,"low_acc":low_acc,"high_acc":high_acc}, ignore_index=True)

               

    print(df_out)

    df_out.to_csv('./results/q3b/average_results.csv', index=False)


def plot_results():
    #mean and std bars
    df = pd.read_csv('./results/question2_a/average_results.csv')
    print(df.head())
    mins =  df["loss_low"]
    maxes = df["loss_high"]
    means = df['loss_mean']
    std = df['loss_std']

    x_axis = [f'{row["hdim"]} - {row["lb"]} - {row["lr"]}' for index, row in df.iterrows()]

    # create stacked errorbars:
    plt.errorbar(x_axis, means, std, fmt='ok', lw=10, ecolor="green")
    plt.errorbar(x_axis, means, [means - mins, maxes - means],
                ecolor='black')

    plt.yscale("log")
    plt.xticks(rotation=45, ha='right')
    plt.xlabel("Parrameter settings (Hidden dimension - Look back - Learning rate)")
    plt.ylabel("Loss")
    plt.tight_layout()
    plt.show()


    pass


def plot_results_3b():
    #mean and std bars
    df = pd.read_csv('./results/q3b/average_results.csv')
    print(df.head())
    mins =  df["low_acc"]
    maxes = df["high_acc"]
    means = df['mean_acc']
    std = df['std_acc']

    x_axis = [f'{row["hdim"]} - {row["lb"]} - {row["lr"]}' for index, row in df.iterrows()]

    # create stacked errorbars:
    plt.errorbar(x_axis, means, std, fmt='ok', lw=10, ecolor="green")
    plt.errorbar(x_axis, means, [means - mins, maxes - means],
                ecolor='black')

    plt.yscale("log")
    plt.xticks(rotation=45, ha='right')
    plt.xlabel("Parrameter settings (Hidden dimension - Look back - Learning rate)")
    plt.ylabel("Accuracy")
    plt.tight_layout()
    plt.show()


    pass



def plot_losses_2a():

    df = pd.read_csv('./results/question2_a/final_model/model.csv', header=None)
    print(df[1])
    plt.plot(df[1])
    plt.xlabel("Epochs")
    plt.ylabel("Loss")
    plt.show()
    return


def plot_losses_3b():

    df = pd.read_csv('./results/q3b/final_model/model.csv', header=None)
    fig, ax1 = plt.subplots()

    color = 'tab:red'
    ax1.set_xlabel('Epochs')
    ax1.set_ylabel('loss', color=color)
    ax1.plot(df[1], color=color)
    ax1.tick_params(axis='y', labelcolor=color)

    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

    color = 'tab:blue'
    ax2.set_ylabel('Accuracy', color=color)  # we already handled the x-label with ax1
    ax2.plot(df[2], color=color)
    ax2.tick_params(axis='y', labelcolor=color)

    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.show()
    return


def plot_acc_5_lm():
    df = pd.read_csv('./results/q5_models/lm/results/results.csv', header=None)
    # print(df.head())
    
    for lb in [0,2,5]:
        d = df.loc[(df[0] == 'test') & (df[1] == lb)]
        d = d.head(10)
        print(d)
        print(lb)
        print("---------------------")
        plt.plot(d[2], d[3])
        
    plt.show()
    
def plot_acc_5_np():
    df = pd.read_csv('./results/q5_models/np/results/results.csv', header=None)
    
    counts = pd.read_csv('./data/wiki-test.txt', sep='\t')
    sbj = counts["subj_idx"]
    vrb = counts["verb_idx"]
    counts["dependcy_range"] = vrb-sbj
    
    counts = counts["dependcy_range"].value_counts()
    
  
    baseline_pred = {}
    for lb in [0,2,5]:
        d = df.loc[(df[0] == 'test') & (df[1] == lb)]
        

        
        
        d_large = d.loc[df[2]>9]
        ranges = d_large[2]
        
        d = d.head(10)

        for range in d[2]:
            baseline_pred[range] = data_balance2(f'./data/modified/test/dep_range_{range}.txt')

        acc_large = []
        instances = []
       
        for range in ranges:
            ac = d_large.loc[d_large[2] == range][3].values[0]
            
            if not pd.isnull(ac):
                
                acc_large.append(ac)
               
                instances.append(counts[range])
          
        acc_large = np.array(acc_large)  
        instances = np.array(instances)  
        
        total_instances = np.sum(instances)
        
        weighted_average = np.sum(acc_large*instances)/total_instances
        
        d = d.append({0: 'test', 1:lb, 2:10, 3:weighted_average}, ignore_index=True)
        
        plt.plot(d[2], d[3], label=f'LB = {lb}')

   
    plt.fill_between(list(baseline_pred.keys()), list(baseline_pred.values()),step='mid', label='Baseline', alpha=0.4, color='pink')

    # baseline = data_balance()
    # plt.axhline(baseline,color='pink', linestyle='--', label=f'Baseline')
    plt.ylim(0.5, 1)
    plt.legend()
    plt.xlabel("Dependency Range")
    plt.ylabel("Accuracy")
    plt.show()
    

def plot_acc_5_np_advantage():
    df = pd.read_csv('./results/q5_models/np/results/results.csv', header=None)
    
    counts = pd.read_csv('./data/wiki-test.txt', sep='\t')
    sbj = counts["subj_idx"]
    vrb = counts["verb_idx"]
    counts["dependcy_range"] = vrb-sbj
    
    counts = counts["dependcy_range"].value_counts()
    
  
    baseline_pred_ranges = []
    baseline_pred_acc = []
    for lb in [0,2,5]:
        baseline_pred_ranges = []
        baseline_pred_acc = []
        d = df.loc[(df[0] == 'test') & (df[1] == lb)]
        

        
        
        d_large = d.loc[df[2]>9]
        ranges = d_large[2]
        
        d = d.head(10)

        for range in d[2]:
            baseline_pred_ranges.append(range)
            baseline_pred_acc.append(data_balance2(f'./data/modified/test/dep_range_{range}.txt'))
            

        acc_large = []
        instances = []
       
        for range in ranges:
            ac = d_large.loc[d_large[2] == range][3].values[0]
            
            if not pd.isnull(ac):
                
                acc_large.append(ac)
               
                instances.append(counts[range])
          
        acc_large = np.array(acc_large)  
        instances = np.array(instances)  
        
        total_instances = np.sum(instances)
        
        weighted_average = np.sum(acc_large*instances)/total_instances
        
        d = d.append({0: 'test', 1:lb, 2:10, 3:weighted_average}, ignore_index=True)
        
        d3 = np.array(d[3]) 
        baseline_pred_acc.append(0)
        print(baseline_pred_acc)
        baseline_pred_acc = np.array(baseline_pred_acc)
        print(baseline_pred_acc)
        d3 = np.nan_to_num(d3)
        baseline_pred_acc = np.nan_to_num(baseline_pred_acc)

        print(d3)
        print(baseline_pred_acc)


        relative_advantage = d3 - baseline_pred_acc
        plt.plot(d[2][1:-1], relative_advantage[1:-1], label=f'LB = {lb}')

    # plt.fill_between(baseline_pred_ranges, baseline_pred_acc,step='mid', label='Baseline', alpha=0.4, color='pink')

    # baseline = data_balance()
    plt.axhline(0,color='pink', linestyle='--', label=f'Baseline')
    # plt.ylim(0.5, 1)
    plt.legend()
    plt.xlabel("Dependency Range")
    plt.ylabel("Accuracy")
    plt.show()
    
    
    
def q5_mod_data_deprange_5():
    df = pd.read_csv('./data/wiki-train.txt', sep='\t')

    df = df.loc[df['verb_idx'] >= 10]
   
    df.to_csv(f'./data/modified/q5/VG/wiki-train_10.txt', index=False, sep='\t')
    
    

q5_mod_data_deprange_5()

# plot_losses_2a()

# plot_losses_3b()

# plot_acc_5_lm()

# plot_acc_5_np()
# plot_acc_5_np_advantage()

# average_from_seeds_3b()

# plot_results_3b()

